Teaser by Content Type module

The teaser by type module allows an admin to set a different teaser length for each content type. Note that as with the site-wide setting, this only affects new posts or new edits of posts.

Teaser length is set on the individual content type settings pages. Choose 'Default' to use the original site-wide setting for teaser length.

You can
  * set teaser length for each content type on the content types pages.
  * set site-wide teaser length (for content types that do not specify a length) on the post settings page.
  * update teasers on all existing posts with the separate retease module (http://drupal.org/project/retease).


Author:
Joachim Noreiko <joachim.n@gmail.com>